import NavState from './components/menu/context/navState.jsx';
import Footer from './components/Footer/Footer.jsx';
import AppRoutes from './components/AppRoutes.jsx';
import "./index.css";

function App() {
return (
	<>
	<NavState/>  
	<section className="pt-5 p-2 text-center">
	  <AppRoutes/>
	</section>
	<Footer/>
  </>
	);
  }
  export default App;
  
  