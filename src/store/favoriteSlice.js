import {createSlice} from "@reduxjs/toolkit";

const favoriteSlise = createSlice({
    name : "favorite",
    initialState:{
        favorites: [],//JSON.parse(localStorage.getItem("CookiFavorite55")),
        isModal: false
    },
    reducers:{
        actionFavoriteCooki: (state) => {state.favorites = JSON.parse(localStorage.getItem("Favorite"))||[]},
        actionFavorite:(state,{payload})=>{
            if( !state.favorites.some((item => item.article === payload.article)) ){
                state.favorites = [...state.favorites, payload]
            } else {
                state.favorites = state.favorites.filter((item)=>item.article !== payload.article )
            }
            localStorage.setItem("Favorite", JSON.stringify(state.favorites))
        },
        actionModal: (state) => {
            state.isModal = !state.isModal
        }      
    }
})

export const {actionFavorite,actionModal,actionFavoriteCooki} = favoriteSlise.actions
export default favoriteSlise.reducer