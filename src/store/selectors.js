//export const selectorFavorites = (store) => store.app.favorites
//export const selectorIsModal = (store) => store.app.isModal

export const selectorOrder = (store) => store.order.order
export const selectorFavorite = (store) => store.favorite.favorites
export const selectorProducts = (store) => store.product.products
export const selectorProductLoading = (store) => store.product.isLoading
export const selectorFavoriteIsModal = (store) => store.favorite.isModal
export const selectorOrderIsModal = (store) => store.order.isModal