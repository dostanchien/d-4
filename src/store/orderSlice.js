import {createSlice} from "@reduxjs/toolkit";

const orderSlise = createSlice({
    name : "order",
    initialState:{
        order: [],
        isModal: false
    },
    reducers:{
        actionOrderCooki: (state) => {state.order = JSON.parse(localStorage.getItem("Order"))||[]},
        actionOrder:(state,{payload})=>{
            if( !state.order.some((item => item.article === payload.article)) ){
                state.order = [...state.order, payload]
            } else {
                state.order = state.order.filter((item)=>item.article !== payload.article )
            }
            localStorage.setItem("Order", JSON.stringify(state.order))
        },
        actionOrderIsModal: (state) => {
            state.isModal = !state.isModal
        }      
    }
})

export const {actionOrder,actionOrderIsModal,actionOrderCooki} = orderSlise.actions
export default orderSlise.reducer