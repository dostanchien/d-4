import Card from '../../components/Card/Components/Card.jsx';
import {useState} from  "react";
import {useDispatch,useSelector} from "react-redux";
import {selectorFavorite,selectorOrder,selectorOrderIsModal} from "../../store/selectors.js";
import {actionFavorite} from "../../store/favoriteSlice.js";
import {actionOrder,actionOrderIsModal} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";


const HomePage = () => {

//const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const order = useSelector(selectorOrder);
const isOpenOrder=useSelector(selectorOrderIsModal);
const dispatch = useDispatch();

const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))

	const productsItems = favorite.map((item, index) => 
{//console.log(index,item)
 // const isIconFavorite = favorite.some((favorit) => favorit.article === item.article);
  return( 
  <Card   products={item}  key={item.article}
          colorFavorite={"red"}
          onClickIcon={()=>handleFavorites(item)}
          clickFirst={()=>{handelModal(), handleCurrentPost(item)}}
  />
  )
  })//end date.map
  return(    
<>
      <h2>Список обраних</h2>
	{favorite.length==0 ? <>
		<div className="text-center">
				<img src="https://i.pinimg.com/474x/38/43/e9/3843e9698cd78aa52d657301932318bc.jpg" alt="Список обраних пуст"/>
		</div></>
		 : productsItems}
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              cart={order}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default HomePage
