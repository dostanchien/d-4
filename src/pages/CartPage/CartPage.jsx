import CardOrder from '../../components/Card/Components/CardOrder.jsx';
import {useState} from  "react";
import {useDispatch,useSelector} from "react-redux";
import {selectorFavorite,selectorOrder,selectorOrderIsModal} from "../../store/selectors.js";
import {actionFavorite} from "../../store/favoriteSlice.js";
import {actionOrder,actionOrderIsModal} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";


const HomePage = () => {

//const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const order = useSelector(selectorOrder);
const isOpenOrder=useSelector(selectorOrderIsModal);
const dispatch = useDispatch();

const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))

	const productsItems = order.map((item) => 
  { const isIconFavorite = favorite.some((favorit) => favorit.article === item.article);
    return( 
          <CardOrder   products={item}  key={item.article}
                  colorFavorite={!isIconFavorite ? "white" : "red"}//меняем цвета иконок фаворите=проверка
                  onClickIcon={()=>handleFavorites(item)}
                  onClickClose={()=>{handelModal(), handleCurrentPost(item)}}
          />
          )
  })//end date.map
  return(    
<>
      <h2>Кошик</h2>
	{order.length==0 ? <>
		<div className="text-center">
				<img src="https://diplomus.kiev.ua/images/cart_null.gif" alt="Корзина пуста"/>
		</div></>
		 : productsItems}
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              cart={order}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default HomePage
