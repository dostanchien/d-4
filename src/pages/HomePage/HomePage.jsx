import Card from '../../components/Card/Components/Card.jsx';
import {useState,useEffect} from  "react";
import {useDispatch,useSelector} from "react-redux";
import {actionFetchProducts} from "../../store/productsSlice.js";
import {selectorProducts,selectorFavorite,selectorOrder,selectorOrderIsModal} from "../../store/selectors.js";
import { actionFavorite } from "../../store/favoriteSlice.js";
import { actionOrder,actionOrderIsModal} from "../../store/orderSlice.js";
import ModalImage from "../../components/Modal/ModalImage/ModalImage.jsx";


const HomePage = () => {

const date = useSelector(selectorProducts);
const favorite = useSelector(selectorFavorite);
const order = useSelector(selectorOrder);
const isOpenOrder=useSelector(selectorOrderIsModal);
const dispatch = useDispatch();

useEffect(() => {
  dispatch(actionFetchProducts(date));
}, [])

const [currentPost, setCurrentPost] = useState({});
const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)

const handelModal = (is) =>       dispatch(actionOrderIsModal(is))
const handleFavorites = (item) => dispatch(actionFavorite(item))
const handleOrder = (item) =>     dispatch(actionOrder(item))


	const productsItems = date.map((item, index) => 
{//console.log(index,item)
  const isIconFavorite = favorite.some((favorit) => favorit.article === item.article);
  return( 
  <Card   products={item}  key={item.article}
          colorFavorite={!isIconFavorite ? "white" : "red"}//меняем цвета иконок фаворите=проверка
          onClickIcon={()=>handleFavorites(item)}
          clickFirst={()=>{handelModal(), handleCurrentPost(item)}}
  />
  )
  })//end date.map
  return(    
<>
      <h2>Керамічні обігрівачі</h2>
    {productsItems}
	 <ModalImage
              isOpen={isOpenOrder}
              handleClose={()=>handelModal()}
              products={currentPost}
              cart={order}
              handleOk={()=> {handelModal(), handleOrder(currentPost)}}
    />
</>
  ) 
}
export default HomePage
