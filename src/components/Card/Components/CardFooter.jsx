import PropTypes from 'prop-types'
import styled from 'styled-components';
import Button from "../../Button/Button"

const FooterSt = styled.div`
display: flex;
justify-content: space-around;
align-items: center;
padding: 0.75rem;
background-color: rgba(0, 0, 0, 0.03);
`;

const CardFooter = ({textFirst, textSecondary, clickFirst, clickSecondary}) =>{
    return(
        <FooterSt>
            { textFirst && <Button boxView click={clickFirst}>{textFirst}</Button>  }
            { textSecondary && <Button underlineView click={clickSecondary}>{textSecondary}</Button> }
        </FooterSt>
    )
}
CardFooter.defaultProps = {
	clickFirst: () => {},
	clickSecondary: () => {}
  }
CardFooter.propTypes = {
    textFirst: PropTypes.string,
    textSecondary: PropTypes.string,
    clickFirst: PropTypes.func,
    clickSecondary: PropTypes.func
}

export default CardFooter