/*Кнопка-гамбургер
Иконка гарбургера состоит из 3-х span элементов. При наведении, мы просто меняем длину верхней и нижней полоски. Когда пользователь 
нажимает на кнопку, мы вызываем функцию toggleMenuMode(), которое переключит состояние isMenuOpen.
Если isMenuOpen истинно, тогда кнопке присваивается класс active. Активное состояние превращает кнопку в крестик, 
что достигается двумя трансформациями – поворотом на 45 градусов и перемещением на несколько пикселей. В это же время, средняя 
полоска скрывается.
Создадим новый файл src/components/HamburgerButton.js и добавим следующий код:*/
import React, { useContext } from 'react';
import styled from 'styled-components';
import { MenuContext } from '../context/navState.jsx';

const MenuButton = styled.button`
  display: block;
  transform-origin: 16px 11px;
  float: left;
  margin-right: 29px;
  outline: 0;
  border: 0;
  padding: 12px;
  background: none;

  span {
    transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  }

  :focus {
    border: medium none rgb(111, 255, 176);
    box-shadow: rgb(111, 255, 176) 0 0 2px 2px;
    outline: 0;
  }

  :hover {
    span:nth-of-type(1) {
      width: 33px;
    }

    span:nth-of-type(2) {
      width: 40px;
    }

    span:nth-of-type(3) {
      width: 30px;
    }
  }

  &.active {
    span:nth-of-type(1) {
      transform: rotate(45deg) translate(10px, 10px);
      width: 40px;
    }

    span:nth-of-type(2) {
      opacity: 0;
      pointer-events: none;
    }

    span:nth-of-type(3) {
      transform: rotate(-45deg) translate(7px, -7px);
      width: 40px;
    }
  }
`;

const Bar = styled.span`
  display: block;
  width: 40px;
  height: 5px;
  margin-bottom: 7px;
  background-color: #fff;
`;

const HamburgerButton = () => {
  const { isMenuOpen, toggleMenuMode } = useContext(MenuContext);

  const clickHandler = () => {
    toggleMenuMode();
  };

  return (
    <MenuButton
      className={isMenuOpen ? 'active' : ''}
      aria-label="Открыть главное меню"
      onClick={clickHandler}
    >
      <Bar />
      <Bar />
      <Bar />
    </MenuButton>
  );
};

export default HamburgerButton;
